package com.milimarty.news

import android.app.Application
import com.milimarty.news.utils.Constants
import com.milimarty.news.utils.Utils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NewsApp: Application() {
    override fun onCreate() {
        super.onCreate()
        /**
         * init GLOBAL CONTEXT
         */
        Constants._contextG = applicationContext

        Constants.phoneWidth = Utils.getPhoneWidth()

    }
}