package com.milimarty.news.api

data class Login(var userName: String, var password: String)

data class User(var id: Int, val email: String, var token: String)