package com.milimarty.news.api

import com.milimarty.news.BuildConfig
import com.milimarty.news.areas.news.model.NewsItemModel
import com.milimarty.news.utils.Constants.PARE_PAGE
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    companion object {
        /**
         * ENUM FOR METHODS
         */
        enum class Country(val country: String) {
            US("us")
        }

        const val NEWS_API_KEY: String = BuildConfig.NEWS_API_KEY

    }


    @GET("everything/")
    suspend fun searchNews(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("apiKey") apiKey: String = NEWS_API_KEY,
        @Query("pageSize") perPage: Int = PARE_PAGE,
    ): NewsItemModel


    @GET("top-headlines/")
    suspend fun topHeadLine(
        @Query("country") country: String = Country.US.country,
        @Query("page") page: Int,
        @Query("apiKey") apiKey: String = NEWS_API_KEY,
        @Query("pageSize") perPage: Int = PARE_PAGE,
    ): NewsItemModel

}