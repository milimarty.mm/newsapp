package com.milimarty.news.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}