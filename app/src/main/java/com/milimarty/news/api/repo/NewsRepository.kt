package com.milimarty.news.api.repo


import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.milimarty.news.api.NewsApi
import com.milimarty.news.areas.news.repo.NewsPagingSource
import com.milimarty.news.utils.Constants.PARE_PAGE

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NewsRepository @Inject constructor(private val newsApi: NewsApi) {


    fun searchPhoto(searchText: String) =
        Pager(
            config = PagingConfig(
                pageSize = PARE_PAGE,
                maxSize = 80,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                NewsPagingSource(newsApi, searchText)
            }
        ).liveData

    suspend fun getPhoto(id: String) = null

}