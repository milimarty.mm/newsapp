package com.milimarty.news.areas.news

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.milimarty.news.MainActivity
import com.milimarty.news.R
import com.milimarty.news.areas.news.adapter.NewsAdapter
import com.milimarty.news.areas.news.adapter.NewsLoadingAdapter
import com.milimarty.news.areas.news.viewmodel.NewsViewModel
import com.milimarty.news.databinding.FragmentNewsBinding
import com.milimarty.news.utils.Utils.hideSoftKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsFragment : Fragment(R.layout.fragment_news) {
    /**
     * Values
     */
    private val viewModel by viewModels<NewsViewModel>()

    private var _mBinding: FragmentNewsBinding? = null
    private val mBinding
        get() = _mBinding!!

    private lateinit var newsAdapter: NewsAdapter


    private fun init() {
        /**
         * init Gallery Adapter
         */
        newsAdapter = NewsAdapter {
            val action = NewsFragmentDirections.actionNewsFragmentToNewsDetailsFragment(it)
            findNavController().navigate(action)
        }

        setupRecyclerView()
        /**
         * init search box
         */
        initSearchBox()

        /**
         * Loading View
         */
        initLoadState()

    }

    /**
     * init loading state
     */
    private fun initLoadState() {
        mBinding.fragmentNewsRetryButton.setOnClickListener {
            newsAdapter.retry()
        }

        newsAdapter.addLoadStateListener { it ->
            mBinding.apply {
                fragmentNewsRecyclerViewLoading.isVisible =
                    it.source.refresh is LoadState.Loading
                fragmentNewsRecyclerView.isVisible = it.source.refresh is LoadState.NotLoading
                fragmentNewsErrorTextView.isVisible = it.source.refresh is LoadState.Error
                fragmentNewsRetryButton.isVisible = it.source.refresh is LoadState.Error

                if (it.source.refresh is LoadState.NotLoading && it.append.endOfPaginationReached && newsAdapter.itemCount < 1) {
                    fragmentNewsRecyclerView.isVisible = false
                    fragmentNewsEmptyTextView.isVisible = true
                } else {
                    fragmentNewsEmptyTextView.isVisible = false

                }

            }
        }
    }

    /**
     *init search box
     */
    private fun initSearchBox() {
        (requireActivity() as MainActivity).onBackPressed = {
            if (mBinding.fragmentNewsSearchEditText.isFocused) {
                mBinding.fragmentNewsSearchEditText.clearFocus()
                false
            } else
                true

        }
        mBinding.fragmentNewsSearchEditText.apply {
            imeOptions = EditorInfo.IME_ACTION_SEARCH
            setOnEditorActionListener(OnEditorActionListener { v, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.text.toString().isNotEmpty() || v.text.toString() != " ") {
                        clearFocus()
                        mBinding.fragmentNewsRecyclerView.scrollToPosition(0)
                        viewModel.searchPhotos(v.text.toString())
                        hideSoftKeyboard(mBinding.fragmentNewsSearchEditText)
                        return@OnEditorActionListener true
                    }
                    Toast.makeText(context, "please insert correct text .", Toast.LENGTH_SHORT)
                        .show()

                    return@OnEditorActionListener false
                }
                false
            })

        }

    }

    private fun setupRecyclerView() {


        mBinding.fragmentNewsRecyclerView.apply {


            hasFixedSize()
            adapter = newsAdapter.withLoadStateHeaderAndFooter(
                header = NewsLoadingAdapter { newsAdapter.retry() },
                footer = NewsLoadingAdapter { newsAdapter.retry() }
            )
            layoutManager = LinearLayoutManager(context)

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentNewsBinding.bind(view)
        init()

        viewModel.newsList.observe(viewLifecycleOwner) {
            Log.d("News_FRAGMENT", it.toString())
            newsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}