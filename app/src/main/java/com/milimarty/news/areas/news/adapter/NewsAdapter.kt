package com.milimarty.news.areas.news.adapter

import android.R.attr.*
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.news.R
import com.milimarty.news.areas.news.model.NewsItemModel
import com.milimarty.news.databinding.PhotoItemCardViewBinding
import com.milimarty.news.utils.Constants
import com.milimarty.news.utils.Utils


class NewsAdapter(private val  onItemClickListener:(NewsItemModel.Articles)->Unit) :
    PagingDataAdapter<NewsItemModel.Articles, NewsAdapter.PhotoItemViewHolder>(diffUtils) {

    companion object {
        private val diffUtils = object : DiffUtil.ItemCallback<NewsItemModel.Articles>() {
            override fun areItemsTheSame(
                oldItem: NewsItemModel.Articles,
                newItem: NewsItemModel.Articles
            ): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(
                oldItem: NewsItemModel.Articles,
                newItem: NewsItemModel.Articles
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: PhotoItemViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(it)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoItemViewHolder {
        val binding =
            PhotoItemCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotoItemViewHolder(binding)

    }


    inner class PhotoItemViewHolder(private val binding: PhotoItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                val position =  bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    item?.let {
                        onItemClickListener(it)
                    }
                }
            }

        }

        @SuppressLint("CheckResult")
        fun bind(articlesItem: NewsItemModel.Articles) {
            binding.articlesItem = articlesItem

            /**
             *  Glide for load image from url to image view
             *  @param binding.photoItemImageView
             */
            Glide
                .with(binding.photoItemImageView)
                .load(articlesItem.photoUrl)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.photo_place_holder_img)
                .into(binding.photoItemImageView)
        }

    }


}