package com.milimarty.news.areas.news.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.milimarty.news.databinding.PhotoLoadingFooterBinding

class NewsLoadingAdapter(val retryMethod: () -> Unit) :
    LoadStateAdapter<NewsLoadingAdapter.LoadStateViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val binding =
            PhotoLoadingFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)

    }


    inner class LoadStateViewHolder(private val binding: PhotoLoadingFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.photoLoadingStateRetryButton.setOnClickListener {
                retryMethod()
            }
        }

        fun bind(loadState: LoadState) {
            binding.apply {
                photoLoadingStateLoadingBar.isVisible = loadState is LoadState.Loading
                photoLoadingStateRetryButton.isVisible = loadState !is LoadState.Loading
                photoLoadingStateTextView.isVisible = loadState !is LoadState.Loading
            }
        }

    }


}