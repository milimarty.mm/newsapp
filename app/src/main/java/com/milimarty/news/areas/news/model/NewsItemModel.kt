package com.milimarty.news.areas.news.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsItemModel(
    @SerializedName("status")
    val status: String,
    @SerializedName("totalResults")
    val totalResults: Long,
    @SerializedName("articles")
    val articles: List<Articles>
) : Parcelable {

    @Parcelize
    data class Articles(
        @SerializedName("source")
        val source: Source,
        @SerializedName("author")
        val author: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("url")
        val url: String,
        @SerializedName("urlToImage")
        val photoUrl: String,
        @SerializedName("publishedAt")
        val publishedAt: String,
        @SerializedName("content")
        val content: String,
        ) : Parcelable {
        @Parcelize
        data class Source(
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String
        ) : Parcelable
    }


}