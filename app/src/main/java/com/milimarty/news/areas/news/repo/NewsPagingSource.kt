package com.milimarty.news.areas.news.repo

import android.util.Log
import androidx.paging.PagingSource
import com.milimarty.news.api.NewsApi
import com.milimarty.news.areas.news.model.NewsItemModel
import com.milimarty.news.utils.Constants
import java.lang.Exception

class NewsPagingSource(
    private val newsApi: NewsApi,
    private val searchText: String
) : PagingSource<Int, NewsItemModel.Articles>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsItemModel.Articles> {

        return try {
            val position = params.key ?: Constants.FIRST_PAGE_INDEX
            val response =   searchText.takeIf { it.isNotEmpty() && it.isNotBlank() }?.let {
                return@let newsApi.searchNews(
                    query = searchText,
                    page = position,
                    perPage = params.loadSize
                )
            }?: kotlin.run{
                return@run  newsApi.topHeadLine(
                    page = position,
                    perPage = params.loadSize
                )
            }

            Log.d("RESPONSE-REQUEST", response.toString())

           return LoadResult.Page(
                data = response.articles ,
                prevKey = if (position == Constants.FIRST_PAGE_INDEX) null else position - 1,
                nextKey = if (response.articles.isEmpty()) null else position + 1

            )

        } catch (e: Exception) {
            e.printStackTrace()
            return  LoadResult.Error(e)
        }


    }
}