package com.milimarty.news.areas.news.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.milimarty.news.api.repo.NewsRepository
import com.milimarty.news.utils.Constants

class NewsViewModel @ViewModelInject constructor(private val repository: NewsRepository) :
    ViewModel() {

    private val searchTextLiveData = MutableLiveData(Constants.DEFAULT_SEARCH_TEXT)
    val newsList = searchTextLiveData.switchMap {
        repository.searchPhoto(it).cachedIn(viewModelScope)
    }

    fun searchPhotos(searchText: String) {
        searchTextLiveData.value = searchText
    }

//
//    private val _listModel = MutableLiveData<Int>(0)
//    val listModel: LiveData<Int>
//        get() = _listModel
//
//    fun setListModel(model: Int) {
//        _listModel.postValue(model)
//    }


}