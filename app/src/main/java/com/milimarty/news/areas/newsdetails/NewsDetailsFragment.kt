package com.milimarty.news.areas.newsdetails

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.news.R
import com.milimarty.news.areas.newsdetails.viewmodel.NewsDetailsViewModel
import com.milimarty.news.databinding.FragmentNewsDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsDetailsFragment : Fragment(R.layout.fragment_news_details) {

    private val viewModel by viewModels<NewsDetailsViewModel>()
    private var _mBinding: FragmentNewsDetailsBinding? = null
    private val mBinding
        get() = _mBinding!!

    /**
     * photoItem
     */
    private val args by navArgs<NewsDetailsFragmentArgs>()

    /**
     * initToolbar
     */
    private fun initToolbar() {
        mBinding.fragmentPhotoToolbar.apply {
            setNavigationIcon(R.drawable.baseline_arrow_back_icon)
            setSubtitleTextColor(ContextCompat.getColor(context, R.color.black))
            setNavigationOnClickListener { findNavController().navigateUp() }
        }
    }

    /**
     * loadData
     */
    private fun loadData() {
        args.article.let {
            mBinding.apply {
                fragmentNewsDetailsAuthorTextView.text = it.author
                fragmentNewsDetailsDescriptionTextView.text = it.description
                fragmentNewsDetailsTitleTextView.text = it.title
                fragmentNewsDetailsPublishedDateTextView.text = it.publishedAt
                fragmentNewsDetailsUrlTextView.text = it.url
            }

        }
        Glide
            .with(mBinding.fragmentPhotoImageView)
            .load(args.article.photoUrl)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.photo_place_holder_img)
            .into(mBinding.fragmentPhotoImageView)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentNewsDetailsBinding.bind(view)

        initToolbar()
        loadData()

    }

    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}