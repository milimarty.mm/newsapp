package com.milimarty.news.areas.newsdetails.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.milimarty.news.api.Response
import com.milimarty.news.api.Status
import com.milimarty.news.api.repo.NewsRepository
import com.milimarty.news.areas.news.model.NewsItemModel
//import com.milimarty.news.areas.newsdetails.model.PhotoDetailsModel
import kotlinx.coroutines.launch
import java.lang.Exception

class NewsDetailsViewModel @ViewModelInject constructor(private val repository: NewsRepository) :
    ViewModel() {

    private val _res = MutableLiveData<Response<NewsItemModel.Articles>>()

    val res: LiveData<Response<NewsItemModel.Articles>>
        get() = _res


    fun getPhoto(photoId: String) = viewModelScope.launch {
        _res.postValue(Response.loading(null))
        var res: Response<NewsItemModel.Articles>
        try {
            val item = repository.getPhoto(id = photoId)
            res = Response(Status.SUCCESS, data = item)
            _res.postValue(res)
        } catch (e: Exception) {
            res = Response(Status.ERROR, message = e.message)
            _res.postValue(res)
        }


    }

}