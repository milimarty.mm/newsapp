package com.milimarty.news.gcomponent.customviews

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.milimarty.news.R
import com.milimarty.news.utils.Constants.customMediumFont
import com.milimarty.news.utils.Utils


class CustomButton : AppCompatButton {


    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!, attrs, defStyle
    ) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        init()
    }

    constructor(context: Context?) : super(context!!) {
        init()
    }

    @SuppressLint("ResourceAsColor")
    private fun init() {
        /**
         * setBackgroundColor
         */
        background = ContextCompat.getDrawable(context, R.drawable.selector_button)

        /**
         * Set Text Color
         */
        setTextColor(Color.BLACK)
        /**
         * set text Size
         */
        textSize = Utils.resourceDpToPx(R.dimen.buttons)


        if (!isInEditMode) {
            typeface = Typeface.createFromAsset(context.assets, customMediumFont)
        }
    }
}