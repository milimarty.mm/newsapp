package com.milimarty.news.gcomponent.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.milimarty.news.utils.Constants.customBoldFont
import com.milimarty.news.utils.Constants.customMediumFont


class CustomTextView : AppCompatTextView {


    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!, attrs, defStyle
    ) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        init()
    }

    constructor(context: Context?) : super(context!!) {
        init()
    }

    private fun init() {
        if (!isInEditMode) {
            typeface = if(typeface.isBold)
                Typeface.createFromAsset(context.assets, customBoldFont)
            else
                Typeface.createFromAsset(context.assets, customMediumFont)

        }
    }
}