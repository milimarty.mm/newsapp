package com.milimarty.news.utils

import android.content.Context

object Constants {
    /**
     * Fonts
     */
    var customMediumFont:String = "font/sf_font.otf"
    var customBoldFont:String = "font/sf_bold_font.otf"

    /**
     * Global Context
     */
    lateinit var _contextG: Context

    /**
     * BASE URL
     */
    const val BASE_URL:String = "http://newsapi.org/v2/"

    var PARE_PAGE:Int = 10

    const val FIRST_PAGE_INDEX = 1

    const val DEFAULT_SEARCH_TEXT = ""

    var phoneWidth:Int = 0


}