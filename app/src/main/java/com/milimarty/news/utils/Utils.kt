package com.milimarty.news.utils

import android.content.Context.INPUT_METHOD_SERVICE
import android.content.res.Resources
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.milimarty.news.utils.Constants._contextG


object Utils {

    fun resourceDpToPx(recourseID: Int): Float {
        return (Constants._contextG.resources.getDimension(recourseID) / _contextG.resources.displayMetrics.density)
    }

    fun dpToPx(dip: Float) = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dip,
        Constants._contextG.resources.displayMetrics
    )

    fun getPhoneWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun hideSoftKeyboard(view: View?) {
        if (view != null) {
            val inputManager: InputMethodManager = view.context
                .getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}