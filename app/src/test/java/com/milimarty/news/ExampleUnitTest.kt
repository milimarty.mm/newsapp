package com.milimarty.news

import android.util.Base64
import com.milimarty.news.api.Login
import com.milimarty.news.api.NewsApi
import com.milimarty.news.api.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Test

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {


    @Test
    fun topHeadline() = runBlocking {


        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.RELEASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val newsApi: NewsApi = retrofit.create(NewsApi::class.java)




        val job = CoroutineScope(IO).launch {
            val response = newsApi.topHeadLine(page = 1)
            print(response.toString())
        }

        job.join()

    }


    @Test
    fun search() = runBlocking {

        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.RELEASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val newsApi: NewsApi = retrofit.create(NewsApi::class.java)




        val job = CoroutineScope(IO).launch {
            val response = newsApi.searchNews(query = "bitcoin",page = 1)
            print(response.toString())
        }

        job.join()
    }
}